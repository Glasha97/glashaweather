package com.glasha.weather.room;

import com.glasha.weather.utils.App;

import java.util.List;

public class RoomRepository {
    private AppDataBase db = App.getInstance().getDatabase();
    private DaoLocaleTable daoLocaleTable = db.daoLocaleTable();
    private List list = null;


    public List<EntityLocaleTable> getAll() {
        list.clear();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                list = daoLocaleTable.getAll();
            }
        });
        thread.start();
        return list;

    }

    public void insert(final EntityLocaleTable entityLocaleTable) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                daoLocaleTable.insert(entityLocaleTable);
            }
        });
        thread.start();

    }

    public void update(final EntityLocaleTable entityLocaleTable) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                daoLocaleTable.update(entityLocaleTable);
            }
        });
        thread.start();

    }

    public List<EntityLocaleTable> getActiveOrRestCities(final long current) {
        list.clear();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                daoLocaleTable.getActiveOrRestCities(current);
            }
        });

        thread.start();
        return list;
    }

    public List<EntityLocaleTable> hasCity(final String city) {
        list.clear();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                daoLocaleTable.hasCity(city);
            }
        });

        thread.start();

        return list;
    }


}
