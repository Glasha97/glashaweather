package com.glasha.weather.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {EntityLocaleTable.class},version = 1,exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {
    public abstract DaoLocaleTable daoLocaleTable();


}
