package com.glasha.weather.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DaoLocaleTable {

    @Query("SELECT * FROM entityLocaleTable")
    List<EntityLocaleTable> getAll();

    @Insert
    void insert(EntityLocaleTable entityLocaleTable);

    @Query("SELECT * FROM entityLocaleTable WHERE currentCity= :currentCity")
    List<EntityLocaleTable> getActiveOrRestCities(long currentCity);

    @Update
    void update(EntityLocaleTable entityLocaleTable);

    @Query("SELECT * FROM entityLocaleTable WHERE city= :city")
    List<EntityLocaleTable> hasCity(String city);


}
