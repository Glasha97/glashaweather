package com.glasha.weather.room;

import androidx.lifecycle.ViewModel;

import java.util.List;

public class RoomViewModel extends ViewModel {
    private RoomRepository roomRepository = new RoomRepository();


    public List<EntityLocaleTable> getAll() {

        return roomRepository.getAll();
    }
    public void insert(EntityLocaleTable entityLocaleTable){
        roomRepository.insert(entityLocaleTable);
    }
    public void update(EntityLocaleTable entityLocaleTable){
        roomRepository.update(entityLocaleTable);
    }
    public List<EntityLocaleTable> getActiveOrRestCities(final long current){
        return  roomRepository.getActiveOrRestCities(current);
    }
    public List<EntityLocaleTable> hasCity(final String city){
        return roomRepository.hasCity(city);
    }


}
