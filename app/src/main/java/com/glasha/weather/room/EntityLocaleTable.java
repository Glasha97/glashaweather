package com.glasha.weather.room;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.glasha.weather.utils.Constant;

@Entity
public class EntityLocaleTable {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private double latitude;
    private double longitude;
    private String city;
    private int currentCity;

    @Ignore
    public EntityLocaleTable(double latitude, double longitude, String city, int currentCity) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.currentCity = currentCity;
    }
    @Ignore
    public EntityLocaleTable(String city, int currentCity) {
        this(-1, -1, city, currentCity);
    }

    public EntityLocaleTable(long id, double latitude, double longitude, String city, int currentCity) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.currentCity = currentCity;
    }

    @Ignore
    public EntityLocaleTable(double latitude, double longitude, int currentCity) {
        this(latitude, longitude, Constant.NOT_FOUNDED_CITY, currentCity);
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(int currentCity) {
        this.currentCity = currentCity;
    }
}





