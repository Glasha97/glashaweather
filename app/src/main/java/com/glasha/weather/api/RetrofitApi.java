package com.glasha.weather.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.glasha.weather.utils.Constant.BASE_URL;

public class RetrofitApi {
    private static RetrofitApi mInstance;
    private Retrofit mRetrofit;


    private WeatherApi loginService;


    private RetrofitApi() {
        //  buildRetrofit(BASE_URL);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }

    public static RetrofitApi getmInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitApi();
        }

        return mInstance;
    }


    public WeatherApi getWeatherService() {
        return mRetrofit.create(WeatherApi.class);
    }
}
