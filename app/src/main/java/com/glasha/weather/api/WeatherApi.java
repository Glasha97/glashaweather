package com.glasha.weather.api;

import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;
import com.glasha.weather.Data.DataApiStats.DataStatWeather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi   {

    @GET("/data/2.5/forecast")
    Call<DataStatWeather> getWEatherAllStatbyGeo(@Query("lat") Double lat, @Query("lon") Double lon, @Query("units")String metric, @Query("lang") String lang, @Query("appid") String apikey);

    @GET("/data/2.5/forecast")
    Call<DataStatWeather> getWEatherAllStatbyCity(@Query("q") String city,  @Query("units")String metric,@Query("lang") String lang, @Query("appid") String apikey);

    @GET("/data/2.5/weather ")
    Call<DataWeatherApi> getCurrentWeatherByGeo(@Query("lat") Double lat, @Query("lon") Double lon, @Query("units")String metric,@Query("lang") String lang, @Query("appid") String apikey);

    @GET("/data/2.5/weather ")
    Call<DataWeatherApi> getCurrentWeatherByCity(@Query("q") String city, @Query("units")String metric,@Query("lang") String lang, @Query("appid") String apikey);


}
