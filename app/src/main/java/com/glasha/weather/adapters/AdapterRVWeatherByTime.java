package com.glasha.weather.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glasha.weather.Data.DataApiStats.ListStat;
import com.glasha.weather.Data.DataApiStats.MainStat;
import com.glasha.weather.Data.DataApiStats.WeatherStat;
import com.glasha.weather.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterRVWeatherByTime extends RecyclerView.Adapter<AdapterRVWeatherByTime.WeatherByTimeVH> {

    private ArrayList<ListStat> list;
    private Context context;
    private OnPositionClickListener onPositionClickListener;
    private int maxTemp;
    private int minTemp;
    private Long sunrise;
    private Long sunset;

    public void setOnPositionClickListener(OnPositionClickListener onPositionClickListener) {
        this.onPositionClickListener = onPositionClickListener;
    }


    public AdapterRVWeatherByTime(ArrayList<ListStat> list, Context context, int maxTemp, int minTemp,Long sunrise,Long sunset) {
        this.list = list;
        this.context = context;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    @NonNull
    @Override
    public WeatherByTimeVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterRVWeatherByTime.WeatherByTimeVH holder = null;
        holder = new WeatherByTimeVH(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rv_bytime, viewGroup, false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherByTimeVH weatherByTimeVH, int i) {
        ListStat data = list.get(weatherByTimeVH.getAdapterPosition());
        Double temp;
        MainStat main=data.getMain();
        temp=main.getTemp();
        Integer tempInt=temp.intValue();
        Float pbHeight;
        RelativeLayout.LayoutParams params = null;
        int degress;
        int minAbsTemp=minTemp*(-1);
        if(maxTemp>minAbsTemp){
            degress=maxTemp;

        }else {
            degress=minAbsTemp;
        }

        if(tempInt==0){
           int tempn=1;
             params = new RelativeLayout.LayoutParams(20, tempn);

        }else if(tempInt<0){
            temp=temp*(-1);

            int plusminTemp=degress;
            pbHeight=temp.floatValue()/plusminTemp*50f;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            float newpbHeight=metrics.density*pbHeight;
            int pbHeightInt=(int)(newpbHeight+0.5f);

            float bottomMargin=50f;
            float toPixelsMargnBottom=metrics.density*bottomMargin;
            int intMaginBottom=(int)(toPixelsMargnBottom);
            int height= intMaginBottom-pbHeightInt;
                    params = new RelativeLayout.LayoutParams(10, pbHeightInt);
            ConstraintLayout.LayoutParams relativeParams = (ConstraintLayout.LayoutParams) weatherByTimeVH.relativeLayout.getLayoutParams();
            relativeParams.setMargins(0,0,0,height);
        }else {
            pbHeight=tempInt.floatValue()/degress*50f;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            float fpixels = metrics.density * pbHeight;
            int pixels = (int) (fpixels + 0.5f);
            params = new RelativeLayout.LayoutParams(10, pixels);
        }














        String testStr=tempInt.toString();
        String s=data.getDtTxt();
        s=s.substring(s.length()-8,s.length()-3);
        List<WeatherStat> weatherStat=data.getWeather();
        String kindOfWeather=  weatherStat.get(0).getMain();


        if(kindOfWeather.equals("Clear")){
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.sun));





        }else if(kindOfWeather.equals("Rain")){
            kindOfWeather="Дождь";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rain));

        }else if(kindOfWeather.equals("Snow")){
            kindOfWeather="Снег";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.snow));

        }else if(kindOfWeather.equals("Thunderstorm")){
            kindOfWeather="Гроза";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.thunderstorm));

        }else if(kindOfWeather.equals("Drizzle")){
            kindOfWeather="Мелкий дождь";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.rain));

        }else if(kindOfWeather.equals("Mist")){
            kindOfWeather="Туман";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.haze));



        }else if(kindOfWeather.equals("Clouds")){
            kindOfWeather="Облачность";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.clouds));

        }else if(kindOfWeather.equals("Fog") || kindOfWeather.equals("Smoke")){
            kindOfWeather="Туман";
            weatherByTimeVH.imageView.setBackground(ContextCompat.getDrawable(context, R.drawable.haze));


        }


//         if(tempInt==0){
//             pbHeight=1.0;
//         }else if(tempInt<0){
//             temp*=(-1);
//              pbHeight=tempInt.doubleValue()/maxTemp*150;
//
//         }

        weatherByTimeVH.textViewTemp.setText(testStr+(char) 0x00B0);
        weatherByTimeVH.textViewTime.setText(s);
       // Log.d("cfc",pbHeight+"");

        weatherByTimeVH.progressBar.setLayoutParams(params);



    }

    @Override
    public int getItemCount() {
        return 9;
    }

    public class WeatherByTimeVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textViewTime;
        public TextView textViewTemp;
        public ProgressBar progressBar;
        public RelativeLayout relativeLayout;
        public ImageView imageView;

        public WeatherByTimeVH(@NonNull View itemView) {
            super(itemView);
            textViewTime = itemView.findViewById(R.id.timebytime);
            textViewTemp = itemView.findViewById(R.id.tempbytime);
            progressBar=itemView.findViewById(R.id.pbPlusTemp);
            relativeLayout=itemView.findViewById(R.id.relLayout);
            imageView=itemView.findViewById(R.id.imageKindOfWeather);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            if (onPositionClickListener != null) {
                onPositionClickListener.onPositionClickListener(AdapterRVWeatherByTime.this.getItemId(getAdapterPosition()));
            }
        }
    }
}
