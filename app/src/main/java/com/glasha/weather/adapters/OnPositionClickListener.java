package com.glasha.weather.adapters;

import com.glasha.weather.Data.DataLocale.DataLocale;
import com.glasha.weather.room.EntityLocaleTable;

public interface OnPositionClickListener {
    void onPositionClickListener(long id);
    void onPositionClickListenerWithCity(long id, EntityLocaleTable DataLocale);

}
