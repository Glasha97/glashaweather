package com.glasha.weather.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.glasha.weather.Data.DataApiStats.ListStat;
import com.glasha.weather.Data.DataApiStats.MainStat;
import com.glasha.weather.R;


import java.util.ArrayList;

import static com.glasha.weather.utils.Utils.fromStringToLongDate;

public class AdapterRVWetherByDay extends RecyclerView.Adapter<AdapterRVWetherByDay.WeatherByDayVH> {
    private ArrayList<ArrayList<ListStat>> list;
    private Context context;
    private OnPositionClickListener onPositionClickListener;
    private Double maxTempPlus;
    private Double maxTempMinus;
    private Double minTempPlus;
    private Double minTempMinus;
    private String sunrise;
    private String sunset;

    public AdapterRVWetherByDay(ArrayList<ArrayList<ListStat>> list, Context context, Double maxTempPlus, Double maxTempMinus, Double minTempPlus, Double minTempMinus, String sunrise, String sunset) {
        this.list = list;
        this.context = context;
        this.maxTempPlus = maxTempPlus;
        this.maxTempMinus = maxTempMinus;
        this.minTempPlus = minTempPlus;
        this.minTempMinus = minTempMinus;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    @NonNull
    @Override
    public WeatherByDayVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterRVWetherByDay.WeatherByDayVH holder=null;
        holder=new WeatherByDayVH(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rv_byday,viewGroup,false));
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherByDayVH weatherByDayVH, int i) {
        ArrayList<ListStat> data = list.get(weatherByDayVH.getAdapterPosition());
       String dataByDay= data.get(0).getDtTxt().substring(5,10);
        dataByDay=  dataByDay.replace("-","");
        char[] charDataByDAy=dataByDay.toCharArray();
        char temp;
        for(int j=0;j<2;j++){
               temp=charDataByDAy[j];
            charDataByDAy[j]=charDataByDAy[j+2];
            charDataByDAy[j+2]=temp;
        }

        String s=new String(charDataByDAy);
        s=s.substring(0,2)+"."+s.substring(2);
        weatherByDayVH.timebyday.setText(s);
        Double tempatDay=0.0;
        Double tempatNight=0.0;
        int size=data.size();
        int day=0;
        int night=0;
        for (ListStat listStat:data){
            MainStat main =listStat.getMain();
        String needData=  listStat.getDtTxt();
            needData=needData.replace(":","");
            //Log.d("cfc",needData);
            needData=needData.substring(11);
           if(fromStringToLongDate(needData)< fromStringToLongDate(sunrise)){
            tempatNight+=main.getTemp();
            ++night;
           }
           else
           {
               if(fromStringToLongDate(needData)> fromStringToLongDate(sunrise)){
                   tempatDay+=main.getTemp();
                   ++day;

               }else if(fromStringToLongDate(needData)> fromStringToLongDate(sunset)){
                   tempatNight+=main.getTemp();
                   ++night;

               }

           }

        }
        tempatDay=tempatDay/day;
        tempatNight=tempatNight/night;

        RelativeLayout.LayoutParams params = null;

        //  Log.d("cfc","TEMP=== "+tempatDay+"   "+tempatNight+"  day= "+day+"  night= "+night);
        int inttempatDay=tempatDay.intValue();
        int inttempatNight=tempatNight.intValue();

        Double degress;
        Double minAbsTemp=minTempPlus*(-1);
        Float pbHeight;

        if(maxTempPlus>minAbsTemp){
            degress=maxTempPlus;

        }else {
            degress=minAbsTemp;
        }
        if(tempatDay==0){
            int tempn=1;
            params = new RelativeLayout.LayoutParams(20, tempn);

        }else if(tempatDay<0){
            tempatDay=tempatDay*(-1);

            Double plusminTemp=degress;
            pbHeight=tempatDay.floatValue()/plusminTemp.floatValue()*50f;

            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            float newpbHeight=metrics.density*pbHeight;
            int pbHeightInt=(int)(newpbHeight+0.5f);

            float bottomMargin=2f;
            float toPixelsMargnBottom=metrics.density*bottomMargin;
            int intMaginBottom=(int)(toPixelsMargnBottom);
            int height= intMaginBottom-pbHeightInt;
            params = new RelativeLayout.LayoutParams(10, pbHeightInt);
            ConstraintLayout.LayoutParams relativeParams = (ConstraintLayout.LayoutParams) weatherByDayVH.relativeLayout.getLayoutParams();
            relativeParams.setMargins(0,0,0,height);
        }else {
            pbHeight=tempatDay.floatValue()/degress.floatValue()*50f;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            float fpixels = metrics.density * pbHeight;
            int pixels = (int) (fpixels + 0.5f);
            params = new RelativeLayout.LayoutParams(10, pixels);
        }
        weatherByDayVH.pbPlusTempDayby.setLayoutParams(params);



        weatherByDayVH.tempbydayPlus.setText(inttempatDay+""+(char) 0x00B0);
        weatherByDayVH.tempForNight.setText(inttempatNight+""+(char) 0x00B0);



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WeatherByDayVH extends RecyclerView.ViewHolder {
        public TextView timebyday;
        public ImageView imageKindOfWeatherByDay;
        public TextView tempbydayPlus;
        public ProgressBar pbPlusTempDayby;
        public ProgressBar pbMinusTempDayby;
        public TextView tempForNight;
        public RelativeLayout relativeLayout;

        public WeatherByDayVH(@NonNull View itemView) {
            super(itemView);
            timebyday=itemView.findViewById(R.id.timebyday);
            imageKindOfWeatherByDay=itemView.findViewById(R.id.imageKindOfWeatherByDay);
            tempbydayPlus=itemView.findViewById(R.id.tempbydayPlus);
            pbPlusTempDayby=itemView.findViewById(R.id.pbPlusTempDayby);
         //   pbMinusTempDayby=itemView.findViewById(R.id.pbMinusTempDayby);
            tempForNight=itemView.findViewById(R.id.tempForNight);
            relativeLayout=itemView.findViewById(R.id.rellayout);

        }
    }
}
