package com.glasha.weather.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;
import com.glasha.weather.Data.DataApiCurrentWeather.Main;
import com.glasha.weather.Data.DataLocale.DataLocale;
import com.glasha.weather.R;
import com.glasha.weather.api.WeatherApi;
import com.glasha.weather.room.EntityLocaleTable;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.glasha.weather.utils.Constant.API_KEY;
import static com.glasha.weather.utils.Constant.BASE_URL;

public class AdapterRVListOfCity extends RecyclerView.Adapter<AdapterRVListOfCity.ListOfCityVH>{

    private ArrayList<EntityLocaleTable> list;
    private Context context;
    public OnPositionClickListener onPositionClickListener;


    public AdapterRVListOfCity(ArrayList<EntityLocaleTable> list, Context context) {
        this.list = list;
        this.context = context;
    }
    public void setOnPositionClickListener(OnPositionClickListener onPositionClickListener) {
        this.onPositionClickListener = onPositionClickListener;
    }



    @NonNull
    @Override
    public AdapterRVListOfCity.ListOfCityVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AdapterRVListOfCity.ListOfCityVH holder = null;
        holder = new AdapterRVListOfCity.ListOfCityVH(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rv_city_name, viewGroup, false));

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdapterRVListOfCity.ListOfCityVH listOfCityVH, int i) {
        EntityLocaleTable dataListOfCity=list.get(listOfCityVH.getAdapterPosition());
        final String city=dataListOfCity.getCity();
        String upperCity=city.substring(0,1).toUpperCase()+city.substring(1);
        listOfCityVH.citytv.setText(upperCity);
        double latitude=dataListOfCity.getLatitude();
        double longitude=dataListOfCity.getLongitude();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WeatherApi weatherApiGetCurrent=retrofit.create(WeatherApi.class);
        Log.d("cfc","long and lan--------------------------"+latitude+"  "+longitude);
        if (latitude<=0 || longitude<=0){
            Call<DataWeatherApi> callCurrent=weatherApiGetCurrent.getCurrentWeatherByCity(city,"metric","ua",API_KEY);
            callCurrent.enqueue(new Callback<DataWeatherApi>() {
                @Override
                public void onResponse(Call<DataWeatherApi> call, Response<DataWeatherApi> response) {
                    Log.d("cfc","secongCall");
                    DataWeatherApi data=response.body();
                    Main main= data.getMain();
                    Double temp=main.getTemp();
                    int tempint=temp.intValue();
                    listOfCityVH.temptv.setText(""+tempint+(char) 0x00B0);

                }

                @Override
                public void onFailure(Call<DataWeatherApi> call, Throwable t) {
                    Log.d("cfc","ErrorsecongCall"+t.toString());

                }
            });

        }else {
            Call<DataWeatherApi> callCurrent=weatherApiGetCurrent.getCurrentWeatherByGeo(latitude,longitude,"metric","ua",API_KEY);
            callCurrent.enqueue(new Callback<DataWeatherApi>() {
                @Override
                public void onResponse(Call<DataWeatherApi> call, Response<DataWeatherApi> response) {
                    Log.d("cfc","secongCall");
                    DataWeatherApi data=response.body();
                    Main main= data.getMain();
                    Double temp=main.getTemp();
                    int tempint=temp.intValue();
                    listOfCityVH.temptv.setText(""+tempint+(char) 0x00B0);



                }

                @Override
                public void onFailure(Call<DataWeatherApi> call, Throwable t) {
                    Log.d("cfc","ErrorsecongCall"+t.toString());

                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ListOfCityVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView citytv;
        public TextView temptv;
        public ListOfCityVH(@NonNull View itemView) {
            super(itemView);
            citytv=itemView.findViewById(R.id.nameOfCity);
            temptv=itemView.findViewById(R.id.idtemp);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onPositionClickListener != null) {
                onPositionClickListener.onPositionClickListenerWithCity(getAdapterPosition(),
                        list.get(getAdapterPosition()));

            }
        }
    }
}
