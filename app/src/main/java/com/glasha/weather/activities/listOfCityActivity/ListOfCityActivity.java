package com.glasha.weather.activities.listOfCityActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;

import com.glasha.weather.R;
import com.glasha.weather.adapters.AdapterRVListOfCity;
import com.glasha.weather.adapters.OnPositionClickListener;
import com.glasha.weather.databinding.ActivityCityOfListBinding;
import com.glasha.weather.room.AppDataBase;
import com.glasha.weather.room.DaoLocaleTable;
import com.glasha.weather.room.EntityLocaleTable;
import com.glasha.weather.utils.App;
import com.glasha.weather.utils.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class ListOfCityActivity extends BaseActivity<ActivityCityOfListBinding> {
    private RecyclerView recyclerView;
    private AdapterRVListOfCity adapter;

    private AppDataBase db = App.getInstance().getDatabase();
    private DaoLocaleTable daoLocaleTable = db.daoLocaleTable();

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.map) {

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.mebu_add_toolbar,menu);


        return true;
    }

    @Override
    public int getLayout() {
        return R.layout.activity_city_of_list;
    }

    @Override
    public Toolbar buildToolbar() {
        return (androidx.appcompat.widget.Toolbar)binding.toolbarListOfCityActivity;
    }

    @SuppressLint("WrongConstant")
    @Override
    public void setupBinding(ActivityCityOfListBinding activityCityOfListBinding) {

        recyclerView = findViewById(R.id.rvListOfCity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                List data = daoLocaleTable.getAll();
                ArrayList list = new ArrayList(data);

                adapter = new AdapterRVListOfCity(list, ListOfCityActivity.this);
                adapter.setOnPositionClickListener(new OnPositionClickListener() {
                    @Override
                    public void onPositionClickListener(long id) {
                        // Toast.makeText(ListOfCityActivity.this, id+"  lol ",Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onPositionClickListenerWithCity(long id, final EntityLocaleTable dataLocale) {
                        // Toast.makeText(ListOfCityActivity.this, id+" currentList  "+
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {


                                List<EntityLocaleTable> activeCity = daoLocaleTable.getActiveOrRestCities(1);
                                for (EntityLocaleTable item : activeCity) {
                                    item.setCurrentCity(0);
                                    daoLocaleTable.update(item);
                                }
                                EntityLocaleTable entityLocaleTable = new EntityLocaleTable(dataLocale.getId(),dataLocale.getLatitude(), dataLocale.getLongitude(), dataLocale.getCity().toLowerCase(), 1);
                                daoLocaleTable.update(entityLocaleTable);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                });
                            }
                        });
                        thread.start();
                    }


                });
                recyclerView.setAdapter(adapter);
            }
        });
        thread.start();

        setSupportActionBar((androidx.appcompat.widget.Toolbar) binding.toolbarListOfCityActivity);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



    }




}
