package com.glasha.weather.activities.mainActivity;

import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;
import com.glasha.weather.Data.DataApiStats.DataStatWeather;
import com.glasha.weather.activities.mainActivity.interfaces.OnGetCurrentWeather;
import com.glasha.weather.activities.mainActivity.interfaces.OnGetStatWether;
import com.glasha.weather.api.RetrofitApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.glasha.weather.utils.Constant.API_KEY;

public class RepositoryMainActivity {
    public void getCurrentWeatherByGeo(Double lat, Double log, final OnGetCurrentWeather onGetCurrentWeater) {

        RetrofitApi.getmInstance().getWeatherService().getCurrentWeatherByGeo(lat, log, "metric", "ua", API_KEY).enqueue(new Callback<DataWeatherApi>() {
            @Override
            public void onResponse(Call<DataWeatherApi> call, Response<DataWeatherApi> response) {
                DataWeatherApi data = response.body();

                onGetCurrentWeater.onSuccess(data);
            }

            @Override
            public void onFailure(Call<DataWeatherApi> call, Throwable t) {
                onGetCurrentWeater.onError(t);

            }
        });

    }

    public void getCurrentWeathreByCity(String city, final OnGetCurrentWeather onGetCurrentWeater) {
        RetrofitApi.getmInstance().getWeatherService().getCurrentWeatherByCity(city, "metric", "ua", API_KEY).enqueue(new Callback<DataWeatherApi>() {
            @Override
            public void onResponse(Call<DataWeatherApi> call, Response<DataWeatherApi> response) {
                DataWeatherApi data = response.body();
                onGetCurrentWeater.onSuccess(data);

            }

            @Override
            public void onFailure(Call<DataWeatherApi> call, Throwable t) {
                onGetCurrentWeater.onError(t);

            }
        });
    }

    public void getStatWeatherByGeo(Double lat, Double log, final OnGetStatWether onGetStatWether) {
        RetrofitApi.getmInstance().getWeatherService().getWEatherAllStatbyGeo(lat, log, "metric", "ua", API_KEY).enqueue(new Callback<DataStatWeather>() {
            @Override
            public void onResponse(Call<DataStatWeather> call, Response<DataStatWeather> response) {
                DataStatWeather data = response.body();
                onGetStatWether.onSuccess(data);
            }

            @Override
            public void onFailure(Call<DataStatWeather> call, Throwable t) {
                onGetStatWether.onError(t);
            }
        });
    }
    public void getStatWeaterByCity(String city,final OnGetStatWether onGetStatWether){
        RetrofitApi.getmInstance().getWeatherService().getWEatherAllStatbyCity(city,"metric", "ua", API_KEY).enqueue(new Callback<DataStatWeather>() {
            @Override
            public void onResponse(Call<DataStatWeather> call, Response<DataStatWeather> response) {
                DataStatWeather data = response.body();

                onGetStatWether.onSuccess(data);

            }

            @Override
            public void onFailure(Call<DataStatWeather> call, Throwable t) {
                onGetStatWether.onError(t);

            }
        });
    }


}
