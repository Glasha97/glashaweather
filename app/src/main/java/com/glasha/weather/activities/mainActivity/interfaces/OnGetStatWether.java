package com.glasha.weather.activities.mainActivity.interfaces;

import com.glasha.weather.Data.DataApiStats.DataStatWeather;

public interface OnGetStatWether {
    void onSuccess(DataStatWeather dataStatWeather);
    void onError(Throwable error);
}
