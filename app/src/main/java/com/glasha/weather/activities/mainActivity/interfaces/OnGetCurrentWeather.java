package com.glasha.weather.activities.mainActivity.interfaces;

import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;

public interface OnGetCurrentWeather {
    void onSuccess(DataWeatherApi dataWeatherApi);
    void onError(Throwable error);
}
