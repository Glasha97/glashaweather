package com.glasha.weather.activities.mainActivity;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;
import com.glasha.weather.Data.DataApiStats.DataStatWeather;
import com.glasha.weather.activities.mainActivity.interfaces.OnGetCurrentWeather;
import com.glasha.weather.activities.mainActivity.interfaces.OnGetStatWether;

public class ViewModelMainActivity extends ViewModel {
    private RepositoryMainActivity repository = new RepositoryMainActivity();
    private MutableLiveData<DataWeatherApi> dataWeatherApiMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<DataStatWeather> dataStatWeatherMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Throwable> errorLiveData = new MutableLiveData<>();

    private ObservableBoolean visibilityPB = new ObservableBoolean(false);


    public ObservableBoolean getVisibilityPB() {
        return visibilityPB;
    }

    public void setVisibilityPB(ObservableBoolean visibilityPB) {
        this.visibilityPB = visibilityPB;
    }

    public MutableLiveData<DataWeatherApi> getDataWeatherApiMutableLiveData() {
        return dataWeatherApiMutableLiveData;
    }

    public MutableLiveData<DataStatWeather> getDataStatWeatherMutableLiveData() {
        return dataStatWeatherMutableLiveData;
    }

    public MutableLiveData<Throwable> getErrorLiveData() {
        return errorLiveData;
    }


    public void getCurrentWeatherByGeo(Double lat, Double log) {
        if (dataWeatherApiMutableLiveData.getValue() == null) {
            repository.getCurrentWeatherByGeo(lat, log, currentWeatherListener);

        }

    }

    public void getCurrentWeathreByCity(String city) {
        if (dataWeatherApiMutableLiveData.getValue() == null) {
            repository.getCurrentWeathreByCity(city, currentWeatherListener);
        }


    }

    public void getStatWetherByGeo(Double lat, Double log) {
        if (dataStatWeatherMutableLiveData.getValue() == null) {


            visibilityPB.set(true);

            repository.getStatWeatherByGeo(lat, log, statWeatherListener);
        }


    }


    public void getStatWeatherByCity(String city) {
        if (dataStatWeatherMutableLiveData.getValue() == null) {


            visibilityPB.set(true);

            repository.getStatWeaterByCity(city, statWeatherListener);
        }

    }


    private OnGetCurrentWeather currentWeatherListener = new OnGetCurrentWeather() {

        @Override
        public void onSuccess(DataWeatherApi dataWeatherApi) {
            dataWeatherApiMutableLiveData.postValue(dataWeatherApi);

        }

        @Override
        public void onError(Throwable error) {

        }
    };
    private OnGetStatWether statWeatherListener = new OnGetStatWether() {
        @Override
        public void onSuccess(DataStatWeather dataStatWeather) {
            dataStatWeatherMutableLiveData.postValue(dataStatWeather);
            visibilityPB.set(false);
        }

        @Override
        public void onError(Throwable error) {
            errorLiveData.postValue(error);
            visibilityPB.set(false);


        }
    };
}
