package com.glasha.weather.activities.mainActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.glasha.weather.Data.DataApiCurrentWeather.Clouds;
import com.glasha.weather.Data.DataApiCurrentWeather.DataWeatherApi;
import com.glasha.weather.Data.DataApiCurrentWeather.Main;
import com.glasha.weather.Data.DataApiCurrentWeather.Sys;
import com.glasha.weather.Data.DataApiCurrentWeather.Weather;
import com.glasha.weather.Data.DataApiCurrentWeather.Wind;
import com.glasha.weather.Data.DataApiStats.DataStatWeather;
import com.glasha.weather.Data.DataApiStats.ListStat;
import com.glasha.weather.Data.DataApiStats.MainStat;
import com.glasha.weather.R;
import com.glasha.weather.activities.AddNewCityActivity;
import com.glasha.weather.activities.ListOfCityActivity;
import com.glasha.weather.adapters.AdapterRVWeatherByTime;
import com.glasha.weather.adapters.AdapterRVWetherByDay;
import com.glasha.weather.databinding.ActivityMainBinding;
import com.glasha.weather.room.AppDataBase;
import com.glasha.weather.room.DaoLocaleTable;
import com.glasha.weather.room.EntityLocaleTable;
import com.glasha.weather.utils.App;
import com.glasha.weather.utils.IsFirstInput;
import com.glasha.weather.utils.LocationApi;
import com.glasha.weather.utils.base.BaseActivity;
import com.glasha.weather.utils.base.MyDexter;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.glasha.weather.utils.Constant.NOT_FOUNDED_CITY;
import static com.glasha.weather.utils.Constant.REQUEST_CHECK_SETTINGS;
import static com.glasha.weather.utils.Utils.fromStringToLongDate;
import static com.glasha.weather.utils.Utils.myDateFormatSymbols;


public class MainActivity extends BaseActivity<ActivityMainBinding> {


    private Long sunrise;
    private Long sunset;
    private String formattedDateSunRise;
    private String formattedDateSunSet;
    private String kindOfWeather;
    private int cloud;
    private int newpressure;
    private Double windSpeed;
    private int humidity;
    private String city = NOT_FOUNDED_CITY;
    private int tempInt;
    private String dateFormat;
    private double latitude;
    private double longitude;

    //private WeatherProvider weatherProvider = new WeatherProvider(this);
    private LocationApi location;
    private android.location.Location mCurrentLocation;
    private AppDataBase db = App.getInstance().getDatabase();
    private DaoLocaleTable daoLocaleTable = db.daoLocaleTable();

    private ViewModelMainActivity model;

    private Boolean mRequestingLocationUpdates = false;
    private ObservableBoolean visibilityPB = new ObservableBoolean(false);


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.list) {
            Intent intent = new Intent(MainActivity.this, ListOfCityActivity.class);
            startActivity(intent);

        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_main_activity, menu);


        return true;
    }


    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public Toolbar buildToolbar() {
        return (androidx.appcompat.widget.Toolbar) binding.mainToolbar;
    }

    @Override
    public void setupBinding(ActivityMainBinding activityMainBinding) {
        showArrowBackPress();

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddNewCityActivity.class);
                startActivity(intent);
            }
        });

        binding.swipeMainActivity.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                IsFirstInput isFirstInput = new IsFirstInput(MainActivity.this);

                Boolean isFirst = isFirstInput.isFirst();

                if (isFirst) {
                    binding.swipeMainActivity.setRefreshing(false);
                    visibilityPB.set(true);
                    model.setVisibilityPB(visibilityPB);
                    MyDexter.isLocation(MainActivity.this, new MyDexter.CallBackOnPermission() {
                        @Override
                        public void onRranted() {

                            location.setmRequestingLocationUpdates(true);
                            location.getLocation();
                        }

                        @Override
                        public void onDenied() {
                            visibilityPB.set(false);
                            model.setVisibilityPB(visibilityPB);
                            Toast.makeText(MainActivity.this, R.string.toastwarninggps, Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    binding.swipeMainActivity.setRefreshing(false);
                    visibilityPB.set(true);
                    model.setVisibilityPB(visibilityPB);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (daoLocaleTable.getAll().size() == 0) {
                                location.getLocation();
                            } else {

                                List<EntityLocaleTable> currenlList = daoLocaleTable.getActiveOrRestCities(1);
                                EntityLocaleTable currentCity = currenlList.get(0);

                                if (currentCity.getCity().equals(NOT_FOUNDED_CITY)) {
                                    model.getStatWetherByGeo(currentCity.getLatitude(), currentCity.getLongitude());
                                    model.getCurrentWeatherByGeo(currentCity.getLatitude(), currentCity.getLongitude());
                                } else {
                                    Log.d("cfc", currentCity.getCity());
                                    model.getCurrentWeathreByCity(currentCity.getCity());
                                    Log.d("cfc", "2");

                                    model.getStatWeatherByCity(currentCity.getCity());
                                }
                            }
                        }
                    });
                    thread.start();


                }


            }
        });
        location = new LocationApi(MainActivity.this, mRequestingLocationUpdates, mLocationCallback, new LocationApi.OnError() {
            @Override
            public void onError() {
                binding.swipeMainActivity.setRefreshing(false);
                visibilityPB.set(false);
                model.setVisibilityPB(visibilityPB);
            }
        });


        model = ViewModelProviders.of(this).get(ViewModelMainActivity.class);
        binding.setViewModel(model);
        binding.invalidateAll();


        model.getErrorLiveData().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        model.getDataWeatherApiMutableLiveData().observe(this, new Observer<DataWeatherApi>() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onChanged(DataWeatherApi dataWeatherApi) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final List<EntityLocaleTable> currenlList = daoLocaleTable.getActiveOrRestCities(1);
                        final EntityLocaleTable currentCity = currenlList.get(0);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                String upperCity = currentCity.getCity().substring(0, 1).toUpperCase() + currentCity.getCity().substring(1);
                                binding.idcity.setText(upperCity);

                            }
                        });
                    }
                });


                thread.start();
                Sys sys = dataWeatherApi.getSys();
                sunrise = sys.getSunrise();
                sunset = sys.getSunset();
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                long currentTime = timestamp.getTime();
                currentTime = currentTime / 1000L;
                Date dateSunRise = new java.util.Date(sunrise * 1000L);
                Date dateSunSet = new java.util.Date(sunset * 1000L);
                SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm");
                sdf.setTimeZone(java.util.TimeZone.getDefault());
                formattedDateSunRise = sdf.format(dateSunRise);
                formattedDateSunSet = sdf.format(dateSunSet);


                Main main = dataWeatherApi.getMain();
                Double temp = main.getTemp();
                List<Weather> weatherList = dataWeatherApi.getWeather();
                ArrayList<Weather> list = new ArrayList<>(weatherList);

                for (Weather weather : list) {
                    kindOfWeather = weather.getMain();

                }


                if (kindOfWeather.equals("Clear")) {
                    kindOfWeather = "Ясно";
                    if (currentTime - sunrise < 0) {
                        long res = currentTime - sunrise;
                        binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.moon));

                    } else {
                        if (currentTime - sunset > 0) {
                            long res = currentTime - sunset;
                            binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.moon));

                        } else {
                            binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.sun));
                            long res = currentTime - sunrise;

                        }

                    }


                } else if (kindOfWeather.equals("Rain")) {
                    kindOfWeather = "Дождь";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.rain));

                } else if (kindOfWeather.equals("Snow")) {
                    kindOfWeather = "Снег";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.snow));

                } else if (kindOfWeather.equals("Thunderstorm")) {
                    kindOfWeather = "Гроза";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.thunderstorm));

                } else if (kindOfWeather.equals("Drizzle")) {
                    kindOfWeather = "Мелкий дождь";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.rain));

                } else if (kindOfWeather.equals("Mist")) {
                    kindOfWeather = "Туман";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.haze));

                } else if (kindOfWeather.equals("Clouds")) {
                    kindOfWeather = "Облачность";
                    if (currentTime - sunrise < 0) {
                        long res = currentTime - sunrise;
                        binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.cloudynight));

                    } else {
                        if (currentTime - sunset > 0) {
                            long res = currentTime - sunset;
                            binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.cloudynight));

                        } else {
                            binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.cloudyday));
                            long res = currentTime - sunrise;


                        }

                    }


                } else if (kindOfWeather.equals("Fog") || kindOfWeather.equals("Smoke")) {
                    kindOfWeather = "Туман";
                    binding.ivKindOfWeatherMain.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.haze));


                }

                double pressure = main.getPressure();//давление
                Double press = pressure * 0.750062;
                newpressure = press.intValue();
                humidity = main.getHumidity();//влажность
                tempInt = temp.intValue();
                Clouds clouds = dataWeatherApi.getClouds();
                cloud = clouds.getAll();//облачность
                Wind wind = dataWeatherApi.getWind();
                windSpeed = wind.getSpeed();
                dateFormat = new SimpleDateFormat("E, dd MMMM HH:mm", myDateFormatSymbols).format(new Date());
                SimpleDateFormat sdfForUsualTimr = new java.text.SimpleDateFormat("E, dd MMMM HH:mm", myDateFormatSymbols);
                sdfForUsualTimr.setTimeZone(java.util.TimeZone.getDefault());

                binding.idWind.setText(windSpeed + " м/с");
                binding.idHumiditi.setText(humidity + "%");
                binding.idPressure.setText(newpressure + " мм. рт. ст.");
                binding.textView5.setText(cloud + "%");
                binding.idDate.setText(dateFormat);
                binding.idDegrees.setText("" + tempInt + (char) 0x00B0);
                binding.kindofWeather.setText(kindOfWeather);
                binding.sunrise.setText(formattedDateSunRise);
                binding.sunset.setText(formattedDateSunSet);
                binding.swipeMainActivity.setRefreshing(false);


            }
        });

        model.getDataStatWeatherMutableLiveData().observe(this, new Observer<DataStatWeather>() {
            @Override
            public void onChanged(DataStatWeather dataStatWeather) {
                Log.d("cfc", "getDataStatWeatherMutableLiveData---------------------");
                String s = dataStatWeather.getCod();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                binding.rvBytime.setLayoutManager(linearLayoutManager);
                List<ListStat> list = dataStatWeather.getList();
                ArrayList<ListStat> result = new ArrayList<>(list);
                ArrayList<Integer> listTemp = new ArrayList<>();
                for (int i = 0; i < 9; i++) {
                    MainStat main = list.get(i).getMain();
                    Double temp = main.getTemp();
                    int tempInt = temp.intValue();
                    listTemp.add(tempInt);
                }
                int maxTemp = Collections.max(listTemp);
                int minTemp = Collections.min(listTemp);
                AdapterRVWeatherByTime adapterByTime = new AdapterRVWeatherByTime(result, MainActivity.this, maxTemp, minTemp, sunrise, sunset);
                binding.rvBytime.setAdapter(adapterByTime);
                //-------------------------------------------------------------------------------------------------------------------------------

                Map<String, ArrayList<ListStat>> mapWeatherByDay = new HashMap<>();
                formattedDateSunRise = formattedDateSunRise.replace(":", "");
                formattedDateSunSet = formattedDateSunSet.replace(":", "");
                Long res = fromStringToLongDate(formattedDateSunSet);


                int k = 0;
                for (int i = k; i < result.size(); i++) {
                    String temp = result.get(i).getDtTxt().substring(0, 10);
                    i = k;
                    ArrayList<ListStat> tempList = new ArrayList<>();

                    for (int j = k; j < result.size(); j++) {

                        String temp1 = result.get(j).getDtTxt().substring(0, 10);

                        if (temp.equals(temp1)) {
                            tempList.add(result.get(j));

                        } else {

                            if (tempList.size() != 0) {
                                mapWeatherByDay.put(temp, tempList);

                            }

                            break;

                        }
                        if (k == 39) {
                            mapWeatherByDay.put(temp, tempList);

                        }
                        k++;

                    }
                }


                SortedSet<String> keys = new TreeSet<>(mapWeatherByDay.keySet());


                Double[] masDay = new Double[keys.size()];
                Double[] masNight = new Double[keys.size()];

                Double maxTempday = 0.0;
                Double maxTempNight = 100.0;
                Double minTempNight = 100.0;
                Double minTempDay = 0.0;
                ArrayList<ArrayList<ListStat>> sortedList = new ArrayList<>();
                for (String key : keys) {
                    ArrayList<ListStat> index = mapWeatherByDay.get(key);
                    sortedList.add(mapWeatherByDay.get(key));

                    for (ListStat in : index) {
                        String timeByDay = in.getDtTxt().substring(11, 16);
                        timeByDay = timeByDay.replace(":", "");
                        if (fromStringToLongDate(timeByDay) < fromStringToLongDate(formattedDateSunRise)) {
                            MainStat main = in.getMain();
                            if (main.getTemp() > maxTempNight) {
                                maxTempNight = main.getTemp();
                            }

                            if (main.getTemp() < minTempNight) {
                                minTempNight = main.getTemp();
                            }

                        } else {
                            if (fromStringToLongDate(timeByDay) > fromStringToLongDate(formattedDateSunRise)) {
                                MainStat main = in.getMain();

                                if (main.getTemp() > maxTempday) {
                                    maxTempday = main.getTemp();
                                }

                                if (main.getTemp() < minTempNight) {
                                    minTempDay = main.getTemp();
                                }


                            } else if (fromStringToLongDate(timeByDay) > fromStringToLongDate(formattedDateSunSet)) {
                                MainStat main = in.getMain();

                                if (main.getTemp() > maxTempNight) {
                                    maxTempNight = main.getTemp();
                                }

                                if (main.getTemp() < minTempNight) {
                                    minTempNight = main.getTemp();
                                }


                            }


                        }
                    }

                }
                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(MainActivity.this);
                linearLayoutManager1.setOrientation(LinearLayoutManager.HORIZONTAL);
                binding.rvByday.setLayoutManager(linearLayoutManager1);
                AdapterRVWetherByDay adapterByDay = new AdapterRVWetherByDay(sortedList, MainActivity.this, maxTempday, maxTempNight, minTempDay, minTempNight, formattedDateSunRise, formattedDateSunSet);
                binding.rvByday.setAdapter(adapterByDay);
                visibilityPB.set(false);
                model.setVisibilityPB(visibilityPB);
                binding.swipeMainActivity.setRefreshing(false);

            }
        });

        model.getErrorLiveData().observe(MainActivity.this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                visibilityPB.set(false);
                model.setVisibilityPB(visibilityPB);
                binding.swipeMainActivity.setRefreshing(false);
                Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

        IsFirstInput isFirstInput = new IsFirstInput(this);

        Boolean isFirst = isFirstInput.isFirst();

        if (isFirst) {
            visibilityPB.set(true);
            model.setVisibilityPB(visibilityPB);
            MyDexter.isLocation(this, new MyDexter.CallBackOnPermission() {
                @Override
                public void onRranted() {

                    location.setmRequestingLocationUpdates(true);
                    location.getLocation();
                }

                @Override
                public void onDenied() {
                    visibilityPB.set(false);
                    model.setVisibilityPB(visibilityPB);
                    Toast.makeText(MainActivity.this, R.string.toastwarninggps, Toast.LENGTH_SHORT).show();

                }
            });

        } else {
            visibilityPB.set(true);
            model.setVisibilityPB(visibilityPB);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (daoLocaleTable.getAll().size() == 0) {
                        location.getLocation();
                    } else {

                        List<EntityLocaleTable> currenlList = daoLocaleTable.getActiveOrRestCities(1);
                        EntityLocaleTable currentCity = currenlList.get(0);

                        if (currentCity.getCity().equals(NOT_FOUNDED_CITY)) {
                            model.getStatWetherByGeo(currentCity.getLatitude(), currentCity.getLongitude());
                            model.getCurrentWeatherByGeo(currentCity.getLatitude(), currentCity.getLongitude());
                        } else {
                            Log.d("cfc", "1");
                            model.getCurrentWeathreByCity(currentCity.getCity());
                            Log.d("cfc", "2");

                            model.getStatWeatherByCity(currentCity.getCity());
                        }
                    }
                }
            });
            thread.start();


        }


    }


    @Override
    protected void onPause() {
        super.onPause();
        if (location.ismRequestingLocationUpdates())
            location.stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            // location is received
            mCurrentLocation = locationResult.getLastLocation();
            location.stopLocationUpdates();

            IsFirstInput isFirstInput = new IsFirstInput(MainActivity.this);
            isFirstInput.setIsNeedShowTC(false);

            latitude = mCurrentLocation.getLatitude();
            longitude = mCurrentLocation.getLongitude();

            String city = null;
            try {
                city = location.getCity(latitude, longitude);


            } catch (IOException e) {
                e.printStackTrace();
            }
            if (city == null) {
                final EntityLocaleTable localeTable = new EntityLocaleTable(latitude, longitude, NOT_FOUNDED_CITY, 1);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        daoLocaleTable.insert(localeTable);

                    }
                });
                thread.start();
            } else {
                final EntityLocaleTable localeTable = new EntityLocaleTable(latitude, longitude, city.toLowerCase(), 1);
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        daoLocaleTable.insert(localeTable);

                    }
                });
                thread.start();
            }

            model.getCurrentWeatherByGeo(latitude, longitude);
            model.getStatWetherByGeo(latitude, longitude);


        }

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        visibilityPB.set(true);
                        model.setVisibilityPB(visibilityPB);
                        location.getLocation();
int c;
                        break;
                    case Activity.RESULT_CANCELED:
                        visibilityPB.set(false);
                        model.setVisibilityPB(visibilityPB);
                        location.setmRequestingLocationUpdates(false);
                        break;
                }
                break;
        }
    }

}
