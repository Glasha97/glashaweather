package com.glasha.weather.activities.addNewCityActivity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.glasha.weather.databinding.ActivityCityNewAddBinding;
import com.glasha.weather.fragments.FragmentAddCityName;
import com.glasha.weather.R;
import com.glasha.weather.utils.base.BaseActivity;

public class AddNewCityActivity extends BaseActivity<ActivityCityNewAddBinding> {
    private FragmentAddCityName fragmentAddCityName;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.map) {
            Toast.makeText(AddNewCityActivity.this, "Soon", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mebu_add_toolbar, menu);


        return true;
    }

    @Override
    public int getLayout() {
        return R.layout.activity_city_new_add;
    }

    @Override
    public Toolbar buildToolbar() {
          return (androidx.appcompat.widget.Toolbar) binding.addToolbar;
    }

    @Override
    public void setupBinding(ActivityCityNewAddBinding activityCityNewAddBinding) {
        fragmentAddCityName = new FragmentAddCityName();
        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction();

        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.lol, fragmentAddCityName);
        ft.addToBackStack(null);

        ft.commit();

        setSupportActionBar((androidx.appcompat.widget.Toolbar) binding.addToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onBackPressed() {
        finish();
        return;
    }
}
