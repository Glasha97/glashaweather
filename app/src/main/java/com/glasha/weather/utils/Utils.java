package com.glasha.weather.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.databinding.ViewDataBinding;

import com.glasha.weather.Data.DataApiStats.ListStat;
import com.glasha.weather.Data.DataApiStats.MainStat;
import com.glasha.weather.R;
import com.glasha.weather.activities.mainActivity.MainActivity;
import com.glasha.weather.adapters.AdapterRVWetherByDay;

import java.sql.Timestamp;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class Utils {
    public static DateFormatSymbols myDateFormatSymbols = new DateFormatSymbols() {

        @Override
        public String[] getMonths() {
            return new String[]{"января", "февраля", "марта", "апреля", "мая", "июня",
                    "июля", "августа", "сентября", "октября", "ноября", "декабря"};
        }

    };

    public static Long fromStringToLongDate(String date) {

        Long firstPartDate = Long.parseLong(date.substring(0, 2));

        Long secondPartDate = Long.parseLong(date.substring(2));

        Long result = firstPartDate * 60 * 60 + secondPartDate * 60;
        return result;

    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static String cityToUpperCase(String currentCity) {
        String upperCity = currentCity.substring(0, 1).toUpperCase() + currentCity.substring(1);

        return upperCity;
    }

    public static long getCurrentTime() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long currentTime = timestamp.getTime();
        currentTime = currentTime / 1000L;
        return currentTime;
    }

    public static void getKindOfWeather(String kindOfWeather, long currentTime, long sunrise, long sunset, ImageView imageView, TextView textView, Activity activity) {
        Log.d("cfc", "get");
        if (kindOfWeather.equals("Clear")) {
            Log.d("cfc", "get");

            textView.setText("Ясно");
            if (currentTime - sunrise < 0) {
                imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.moon));

            } else {
                if (currentTime - sunset > 0) {
                    imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.moon));

                } else {
                    imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.sun));

                }

            }

        } else if (kindOfWeather.equals("Rain")) {
            textView.setText("Дождь");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.rain));

        } else if (kindOfWeather.equals("Snow")) {
            textView.setText("Снег");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.snow));

        } else if (kindOfWeather.equals("Thunderstorm")) {
            textView.setText("Гроза");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.thunderstorm));

        } else if (kindOfWeather.equals("Drizzle")) {
            textView.setText("Мелкий дождь");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.rain));

        } else if (kindOfWeather.equals("Mist")) {
            textView.setText("Туман");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.haze));

        } else if (kindOfWeather.equals("Clouds")) {
            textView.setText("Облачность");

            if (currentTime - sunrise < 0) {
                imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.cloudynight));

            } else {
                if (currentTime - sunset > 0) {
                    imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.cloudynight));

                } else {
                    imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.cloudyday));

                }
            }

        } else if (kindOfWeather.equals("Fog") || kindOfWeather.equals("Smoke")) {
            textView.setText("Туман");

            imageView.setBackground(ContextCompat.getDrawable(activity, R.drawable.haze));


        }

    }

    public static String getFormattedSunRise(long sunrise) {
        Date dateSunRise = new java.util.Date(sunrise * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        return sdf.format(dateSunRise);
    }

    public static String getFormattedSunSet(long sunset) {
        Date dateSunSet = new java.util.Date(sunset * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm");
        sdf.setTimeZone(java.util.TimeZone.getDefault());
        return sdf.format(dateSunSet);
    }

    public static AdapterRVWetherByDay processData(ArrayList<ListStat> result, String formattedDateSunRise, String formattedDateSunSet, Activity activity) {
        Map<String, ArrayList<ListStat>> mapWeatherByDay = new HashMap<>();

        int k = 0;
        for (int i = k; i < result.size(); i++) {
            String temp = result.get(i).getDtTxt().substring(0, 10);
            i = k;
            ArrayList<ListStat> tempList = new ArrayList<>();

            for (int j = k; j < result.size(); j++) {

                String temp1 = result.get(j).getDtTxt().substring(0, 10);

                if (temp.equals(temp1)) {
                    tempList.add(result.get(j));

                } else {

                    if (tempList.size() != 0) {
                        mapWeatherByDay.put(temp, tempList);

                    }

                    break;

                }
                if (k == 39) {
                    mapWeatherByDay.put(temp, tempList);

                }
                k++;

            }
        }

        SortedSet<String> keys = new TreeSet<>(mapWeatherByDay.keySet());

        Double maxTempday = -1000.0;
        Double maxTempNight = -1000.0;
        Double minTempNight = 1000.0;
        Double minTempDay = 1000.0;
        ArrayList<ArrayList<ListStat>> sortedList = new ArrayList<>();
        for (String key : keys) {
            ArrayList<ListStat> index = mapWeatherByDay.get(key);
            sortedList.add(mapWeatherByDay.get(key));

            for (ListStat in : index) {
                String timeByDay = in.getDtTxt().substring(11, 16);
                timeByDay = timeByDay.replace(":", "");
                if (fromStringToLongDate(timeByDay) < fromStringToLongDate(formattedDateSunRise)) {
                    MainStat main = in.getMain();
                    if (main.getTemp() > maxTempNight) {
                        maxTempNight = main.getTemp();
                    }

                    if (main.getTemp() < minTempNight) {
                        minTempNight = main.getTemp();
                    }

                } else {
                    if (fromStringToLongDate(timeByDay) > fromStringToLongDate(formattedDateSunRise)) {
                        MainStat main = in.getMain();

                        if (main.getTemp() > maxTempday) {
                            maxTempday = main.getTemp();
                        }

                        if (main.getTemp() < minTempNight) {
                            minTempDay = main.getTemp();
                        }

                    } else if (fromStringToLongDate(timeByDay) > fromStringToLongDate(formattedDateSunSet)) {
                        MainStat main = in.getMain();

                        if (main.getTemp() > maxTempNight) {
                            maxTempNight = main.getTemp();
                        }

                        if (main.getTemp() < minTempNight) {
                            minTempNight = main.getTemp();
                        }
                    }
                }
            }
        }
        AdapterRVWetherByDay adapterByDay = new AdapterRVWetherByDay(sortedList, activity, maxTempday, maxTempNight, minTempDay, minTempNight, formattedDateSunRise, formattedDateSunSet);
        return adapterByDay;
    }
}
