package com.glasha.weather.utils.base;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity<T extends ViewDataBinding> extends AppCompatActivity {
    public abstract int getLayout();

    public abstract androidx.appcompat.widget.Toolbar buildToolbar();

    public abstract void setupBinding(T t);

    protected T binding;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayout());
        setupBinding(binding);


    }

    protected void setTittleToolbar(String s) {
        androidx.appcompat.widget.Toolbar toolbar = buildToolbar();
        if (toolbar != null) {
            toolbar.setTitle(s);

        }
    }

    protected void showArrowBackPress() {
        setSupportActionBar(buildToolbar());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }


}
