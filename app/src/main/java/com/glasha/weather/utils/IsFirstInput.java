package com.glasha.weather.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.glasha.weather.utils.Constant.SP_PUT_BOOLEAN;

public class IsFirstInput {
    private SharedPreferences sharedPreferences;

    public boolean isFirst() {
        return sharedPreferences.getBoolean(SP_PUT_BOOLEAN, true);
    }

    public IsFirstInput(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }
    public void setIsNeedShowTC(boolean isNeedShowTC){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(SP_PUT_BOOLEAN, isNeedShowTC);
        editor.apply();
    }
}
