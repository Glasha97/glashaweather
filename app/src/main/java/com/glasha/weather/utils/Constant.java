package com.glasha.weather.utils;

public class Constant {
    public static final String BASE_URL="https://api.openweathermap.org";
    public static final String API_KEY="9daeba3c447d904b73922e79cb6c9617";

    public static final String SP_FIRST_INPUT="SP_FIRST_INPUT";
    public static final String SP_PUT_BOOLEAN="SP_PUT_BOOLEAN";

    public static final String DBNAME  = "DBNAME";

    public static final String DB_LOCATE_TABLE  = "DB_LOCATE_TABLE";
    public static final String LOCALE_ID  = "_LOCALE_ID";
    public static final String LOCALE_LAT  = "_lOCALE_LAT";
    public static final String LOCALE_LOT  = "_lOCALE_LOT";
    public static final String LOCALE_CITY  = "_lOCALE_CITY";
    public static final String CURRENT_CITY  = "CURRENT_CITY";

    public static final int REQUEST_CHECK_SETTINGS = 0x1;

    public static final String NOT_FOUNDED_CITY="NOT_FOUNDED_CITY";

    public static final String SAVED_INSTANCE_STATE_CITY="SAVED_INSTANCE_STATE_CITY";
    public static final String SAVED_INSTANCE_STATE_TEMP="SAVED_INSTANCE_STATE_TEMP";
    public static final String SAVED_INSTANCE_STATE_DATE="SAVED_INSTANCE_STATE_DATE";
    public static final String SAVED_INSTANCE_STATE_HUMIDITY="SAVED_INSTANCE_STATE_HUMIDITY";
    public static final String SAVED_INSTANCE_STATE_WINDSPEED="SAVED_INSTANCE_STATE_WINDSPEED";
    public static final String SAVED_INSTANCE_STATE_PRESSURE="SAVED_INSTANCE_STATE_PRESSURE";
    public static final String SAVED_INSTANCE_STATE_CLOUD="SAVED_INSTANCE_STATE_CLOUD";
    public static final String SAVED_INSTANCE_STATE_KINDOFWEATHER="SAVED_INSTANCE_STATE_KINDOFWEATHER";
    public static final String SAVED_INSTANCE_STATE_SUNRISE="SAVED_INSTANCE_STATE_SUNRISE";
    public static final String SAVED_INSTANCE_STATE_SUNSET="SAVED_INSTANCE_STATE_SUNSET";





}
