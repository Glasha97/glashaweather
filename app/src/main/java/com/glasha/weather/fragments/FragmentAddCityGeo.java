package com.glasha.weather.fragments;

import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.glasha.weather.R;
import com.glasha.weather.databinding.GeoCityAddFragmentBinding;
import com.glasha.weather.room.AppDataBase;
import com.glasha.weather.room.DaoLocaleTable;
import com.glasha.weather.room.EntityLocaleTable;
import com.glasha.weather.utils.App;
import com.glasha.weather.utils.LocationApi;
import com.glasha.weather.utils.base.BaseFragment;

import java.io.IOException;
import java.util.List;


public class FragmentAddCityGeo extends BaseFragment<GeoCityAddFragmentBinding> {
    private FragmentAddCityName fragmentAddCityName;

    private AppDataBase db = App.getInstance().getDatabase();
    private DaoLocaleTable daoLocaleTable = db.daoLocaleTable();

    @Override
    public int getLayout() {
        return R.layout.geo_city_add_fragment;
    }

    @Override
    public void setupBinding(GeoCityAddFragmentBinding geoCityAddFragmentBinding) {
        fragmentAddCityName = new FragmentAddCityName();

        binding.toNameCityFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager()
                        .beginTransaction();

                ft.setCustomAnimations(R.anim.slide_in_geo_fragment, R.anim.slide_out_geo_fragment);

                ft.replace(R.id.lol, fragmentAddCityName);
                ft.addToBackStack(null);

                ft.commit();

            }
        });
        binding.addCityByGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String longitute = binding.longituteEt.getText().toString();
                final String latitude = binding.langituteEt.getText().toString();
                if (latitude.equals("") || longitute.equals("")) {
                    Toast.makeText(getActivity(), "Вы не ввели геоданные", Toast.LENGTH_LONG).show();
                } else {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            LocationApi location = new LocationApi(getActivity());

                            try {
                                String nameCity = location.getCity(Double.valueOf(latitude), Double.valueOf(longitute));
                                List list = daoLocaleTable.hasCity(nameCity.toLowerCase());
                                if (list.size() == 0) {
                                    EntityLocaleTable city = new EntityLocaleTable(Double.valueOf(latitude), Double.valueOf(longitute), nameCity.toLowerCase(), 0);

                                    daoLocaleTable.insert(city);
                                    Toast.makeText(getActivity(), "Новый город успешно добавлен", Toast.LENGTH_LONG).show();
                                    binding.longituteEt.setText("");
                                    binding.langituteEt.setText("");
                                } else {
                                    Toast.makeText(getActivity(), "Данный город уже есть в вашем списке", Toast.LENGTH_LONG).show();

                                }


                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();


                }
            }
        });
    }

}
