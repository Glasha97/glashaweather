package com.glasha.weather.fragments;

import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentTransaction;

import com.glasha.weather.R;
import com.glasha.weather.databinding.NameCityAddFragmentBinding;
import com.glasha.weather.room.AppDataBase;
import com.glasha.weather.room.DaoLocaleTable;
import com.glasha.weather.room.EntityLocaleTable;
import com.glasha.weather.utils.App;
import com.glasha.weather.utils.base.BaseFragment;

import java.util.List;

public class FragmentAddCityName extends BaseFragment<NameCityAddFragmentBinding> {
    private FragmentAddCityGeo fragmentAddCityGeo;

    private AppDataBase db = App.getInstance().getDatabase();
    private DaoLocaleTable daoLocaleTable = db.daoLocaleTable();


    @Override
    public int getLayout() {
        return R.layout.name_city_add_fragment;
    }

    @Override
    public void setupBinding(NameCityAddFragmentBinding nameCityAddFragmentBinding) {
        fragmentAddCityGeo = new FragmentAddCityGeo();

        binding.toGeoFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager()
                        .beginTransaction();

                ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

                ft.replace(R.id.lol, fragmentAddCityGeo);
                ft.addToBackStack(null);

                ft.commit();

            }
        });

        binding.addCityByGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] nameCity = {binding.etGetCityName.getText().toString()};
                if (nameCity[0].equals("")) {
                    Toast.makeText(getActivity(), "Вы не ввели город", Toast.LENGTH_LONG).show();
                } else {
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            List list = daoLocaleTable.hasCity(nameCity[0].toLowerCase());
                            if (list.size() == 0) {
                                nameCity[0] = nameCity[0].replaceAll("\\s+$", "");
                                EntityLocaleTable city = new EntityLocaleTable(-1, -1, nameCity[0].toLowerCase(), 0);
                                daoLocaleTable.insert(city);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), "Новый город успешно добавлен", Toast.LENGTH_LONG).show();
                                        binding.etGetCityName.setText("");
                                    }
                                });

                            } else {
                                getActivity().runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(getActivity(), "Данный город уже есть в вашем списке", Toast.LENGTH_LONG).show();

                                    }
                                });

                            }
                        }
                    });
                    thread.start();
                }

            }
        });
    }


}
