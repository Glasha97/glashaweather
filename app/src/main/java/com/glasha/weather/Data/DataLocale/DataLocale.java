package com.glasha.weather.Data.DataLocale;

public class DataLocale {

    private long id;
    private double latitude;
    private double longitude;
    private String cityInMap;
    private long current;

    public DataLocale(long id, double latitude, double longitude, String cityInMap, long current) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.cityInMap = cityInMap;
        this.current=current;
    }
    public DataLocale(double latitude, double longitude, String cityInMap,long current){
        this(-1,latitude,longitude, cityInMap,current);
    }
    public DataLocale( String cityInMap, long current){
        this(-1,-1,-1, cityInMap,current);
    }


    public long getCurrent() {
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }

    public String getCityInMap() {
        return cityInMap;
    }

    public void setCityInMap(String cityInMap) {
        this.cityInMap = cityInMap;
    }

    public void setId(long id) { this.id = id; }

    public long getId() { return id; }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
