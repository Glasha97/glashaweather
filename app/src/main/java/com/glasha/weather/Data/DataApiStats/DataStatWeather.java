
package com.glasha.weather.Data.DataApiStats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataStatWeather {

    @SerializedName("cod")
    @Expose
    private String cod;
    @SerializedName("message")
    @Expose
    private Double message;
    @SerializedName("cnt")
    @Expose
    private Integer cnt;
    @SerializedName("list")
    @Expose
    private java.util.List<ListStat> list = null;
    @SerializedName("city")
    @Expose
    private CityStat city;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<ListStat> getList() {
        return list;
    }

    public void setList(java.util.List<ListStat> list) {
        this.list = list;
    }

    public CityStat getCity() {
        return city;
    }

    public void setCity(CityStat city) {
        this.city = city;
    }

}
