package com.glasha.weather.Data.DataListOfCity;

public class DataListOfCity {
    private String city;

    public DataListOfCity(String city, Double temp) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


}
