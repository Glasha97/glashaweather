
package com.glasha.weather.Data.DataApiStats;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListStat {

    @SerializedName("dt")
    @Expose
    private Integer dt;
    @SerializedName("main")
    @Expose
    private MainStat main;
    @SerializedName("weather")
    @Expose
    private java.util.List<WeatherStat> weather = null;
    @SerializedName("clouds")
    @Expose
    private CloudsStat clouds;
    @SerializedName("wind")
    @Expose
    private WindStat wind;
    @SerializedName("sys")
    @Expose
    private SysStat sys;
    @SerializedName("dt_txt")
    @Expose
    private String dtTxt;
    @SerializedName("rain")
    @Expose
    private RainStat rain;
    @SerializedName("snow")
    @Expose
    private SnowStat snow;

    public Integer getDt() {
        return dt;
    }

    public void setDt(Integer dt) {
        this.dt = dt;
    }

    public MainStat getMain() {
        return main;
    }

    public void setMain(MainStat main) {
        this.main = main;
    }

    public java.util.List<WeatherStat> getWeather() {
        return weather;
    }

    public void setWeather(java.util.List<WeatherStat> weather) {
        this.weather = weather;
    }

    public CloudsStat getClouds() {
        return clouds;
    }

    public void setClouds(CloudsStat clouds) {
        this.clouds = clouds;
    }

    public WindStat getWind() {
        return wind;
    }

    public void setWind(WindStat wind) {
        this.wind = wind;
    }

    public SysStat getSys() {
        return sys;
    }

    public void setSys(SysStat sys) {
        this.sys = sys;
    }

    public String getDtTxt() {
        return dtTxt;
    }

    public void setDtTxt(String dtTxt) {
        this.dtTxt = dtTxt;
    }

    public RainStat getRain() {
        return rain;
    }

    public void setRain(RainStat rain) {
        this.rain = rain;
    }

    public SnowStat getSnow() {
        return snow;
    }

    public void setSnow(SnowStat snow) {
        this.snow = snow;
    }

}
